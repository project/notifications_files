// $Id $

Drupal.behaviors.notifications_files = function (context) {
  $('.notifications-files-node:not(.notifications_files-processed)', context).addClass('notifications_files-processed').each(function() {
    $('.notifications-files-node').find('input').hide();
  
    $('.notifications-files-node').find('input').each(function(){
      if ($(this).attr('checked')) {
	$(this).parent().addClass('attach');
      }
    });
    $('.notifications-files-node').find('input').click(function(){
      $(this).parent().toggleClass('attach');
    });
  });
};
